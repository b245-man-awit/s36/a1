const express = require("express");
const router = express.Router();

const taskController = require("../Controllers/taskControllers.js")

// Routes


// Route for getAll
router.get("/get",taskController.getAll);

// Route for createTask
router.post("/addTask", taskController.createTask);

// Route for deleteTask
router.delete("/deleteTasks/:id", taskController.deleteTask);

// Activity
    // Route for get spcific task 
    router.get("/:id", taskController.getTask);

    // Route for complete status
    router.put("/:id/complete", taskController.updateStatus);









module.exports = router;