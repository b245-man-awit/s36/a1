const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./Routes/taskRoute.js");

const app = express ();
const port = 3001

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch245-man-awit.8gpp4b2.mongodb.net/s35-discussion?retryWrites=true&w=majority", {
        // allows us to avoid any current and furure errors while connection to MONGODB (pag may future update si mongoDB)
        useNewUrlParser: true,
        useUnifiedTopology: true

    })

    // Check connection
    let db = mongoose.connection

    // error catcher
    db.on("error", console.error.bind(console, "Connection error!"));

    // Confirmation of the connection
    db.once("open", ()=> console.log("We are now connected to the cloud!"))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// routing
// localhost:3001/tasks/get
app.use("/tasks", taskRoute);



app.listen(port, ()=> console.log(`Server is running at port ${port}!`))

// Separation of concerns:
// model > controller > routes > server/application

// 1. Model should be connected to the controller. (Si model dun nakapasok si schema, then always iexport si model; require nya si database which is mongoose)
// 2. Controller should be connected to the Routes. (kunin nya ung database from model; require nya si model)
// 3. Route should be connected to the server/application.(after kay controller si route mgpapatakbo sa kanila so need nya si express package; require nya si route tska si controller. Then export nya si router from the express package)