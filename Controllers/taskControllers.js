const Task = require("../Models/task.js");

// Controllers and functions

// Controller/function to get all the task on our database
module.exports.getAll = (request, response) =>{
    Task.find({}).then(result => {
        return response.send(result)
    })
    .catch(error => {
        return response.send(error)
    })
}

// Add Task on our database
module.exports.createTask = (request, response) => {
    const input = request.body

    Task.findOne({name: input.name})
    .then (result =>{
        if(result !== null){
            return response.send("The task is already existing!")
        }else{
            let newTask = new Task({
                name: input.name
            });

            newTask.save().then(save => {
                return response.send("The task is successfully added!")
            }).catch(error =>{
                return response.send(error)
            })
        }
    })
    .catch(error => {
        return response.send
    })
}

// Controller that will delete the document that contains the given objects

module.exports.deleteTask = (request, response) => {
    let idToBeDeleted = request.params.id;

    // findbyIdAndRemove - to find the document that contains the id and then delte the document
    Task.findByIdAndRemove(idToBeDeleted)
    .then(result => {
        return response.send(result)
    })
    .catch(error => {
        return response.send(error);
    })
}

// Activity: 
    // Controller that will get a specific task
    module.exports.getTask = (request, response) =>{
        let id = request.params.id;
        Task.findOne({_id: id}).then(result => {
            return response.send(result)
        })
        .catch(error => {
            return response.send(error)
        })
    }

    // Controller that will update status to complete
    module.exports.updateStatus = (request, response) => {
        let idToBeUpdated = request.params.id;
    
        Task.findByIdAndUpdate(idToBeUpdated, {
            status: "complete"
        }).then(result => {
            Task.findOne({_id: idToBeUpdated}).then(result => {
                return response.send(result)
            })
        })
        .catch(error => {
            return response.send(error)
        })
    }